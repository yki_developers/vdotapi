<?php
Class M_crontab extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function listReminder(){
        return $this->db->get(DBV_DAFTAR_REMINDER)->result();
    }

    public function updateReminder($id){
        return $this->db->update(DB_USER,array("status_reminder"=>"1"),array("user_idpasien"=>$id));
    }

    
}