<?php
Class M_api extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->sitb= $this->load->database("sitb",TRUE);
    }

    public function getSpecimen($origin,$destination){
   
        $this->sitb->select("b.`nama` AS patient_name,
        c.`kode` AS order_hf_sender,
        d.`kode` AS order_hf_recipient,
        b.`nik` AS patient_nid,
        b.`jenis_kelamin_id` AS patient_sex,
        b.`tanggal_lahir` AS patient_bday,
        a.`id` AS specimen_id,
        a.`umur` AS specimen_patient_age_year,
        a.`umur_bl` AS specimen_patient_age_month,
        a.`no_sediaan` AS specimen_number,
        a.periksa_xpert_id as specimen_xpert,
        a.periksa_lpa_lini2_id as specimen_lpa,
        a.periksa_biakan_id as specimen_biakan,
        a.periksa_paket_standar_id as specimen_cdst,
        a.tgl_pengambilan as specimen_date_collected");
        $this->sitb->from("ta_permohonan_lab a");
        $this->sitb->where("c.kode",$origin);
        $this->sitb->where("d.kode",$destination);
        $this->sitb->where("a.status_terima_id","0");
        $this->sitb->where("a.status_kirim_id","1");
        $this->sitb->where("a.jenis_pengiriman_id","1");
        $this->sitb->join("ta_person b","a.`person_id`=b.`id`");
        $this->sitb->join("ref_unit_tb c","a.fasyankes_id=c.id");
        $this->sitb->join("ref_unit_tb d","a.lab_tujuan_id=d.id");
        //return $this->sitb->get_compiled_select();
        return $this->sitb->get()->result();
    }



    public function getSearchSpecimen($rdata){
        $origin = $rdata['order_hf_sender'];
        $destination = $rdata["order_hf_recipient"];
        $search = $rdata['search'];
   
        $this->sitb->select("b.`nama` AS patient_name,
        c.`kode` AS order_hf_sender,
        d.`kode` AS order_hf_recipient,
        b.`nik` AS patient_nid,
        b.`jenis_kelamin_id` AS patient_sex,
        b.`tanggal_lahir` AS patient_bday,
        a.`id` AS specimen_id,
        a.`umur` AS specimen_patient_age_year,
        a.`umur_bl` AS specimen_patient_age_month,
        a.`no_sediaan` AS specimen_number,
        a.periksa_xpert_id as specimen_xpert,
        a.periksa_lpa_lini2_id as specimen_lpa,
        a.periksa_biakan_id as specimen_biakan,
        a.periksa_paket_standar_id as specimen_cdst,
        a.tgl_pengambilan as specimen_date_collected
        ");
        $this->sitb->from("ta_permohonan_lab a");
        $this->sitb->group_start();
        $this->sitb->where("c.kode",$origin);
        $this->sitb->where("d.kode",$destination);
        $this->sitb->where("a.status_terima_id","0");
        $this->sitb->where("a.status_kirim_id","1");
        $this->sitb->where("a.jenis_pengiriman_id","1");

        //$this->sitb->where("a.periksa_xpert_id","1");
        $this->sitb->group_end();
        $this->sitb->group_start();
        $this->sitb->like('b.nama',$search,'both');
        $this->sitb->or_like("b.nik",$search);
        $this->sitb->or_like("a.no_sediaan",$search);
        $this->sitb->or_like("a.id",$search);
        $this->sitb->group_end();
        $this->sitb->join("ta_person b","a.`person_id`=b.`id`");
        $this->sitb->join("ref_unit_tb c","a.fasyankes_id=c.id");
        $this->sitb->join("ref_unit_tb d","a.lab_tujuan_id=d.id");
        //return $this->sitb->get_compiled_select();
        return $this->sitb->get()->result();
    }


    public function getDetailSpecimen($id){
        $this->sitb->select("b.`nama` AS patient_name,
        c.`kode` AS order_hf_sender,
        d.`kode` AS order_hf_recipient,
        b.`nik` AS patient_nid,
        b.`jenis_kelamin_id` AS patient_sex,
        b.`tanggal_lahir` AS patient_bday,
        e.`kode` AS patient_province,
        f.`kode` AS patient_district,
        
        b.`no_hp` AS patient_phone_number,
        b.`alamat_domisili` AS patient_address, 
        a.`id` AS specimen_id,
        a.`umur` AS specimen_patient_age_year,
        a.`umur_bl` AS specimen_patient_age_month,
        a.`no_sediaan` AS specimen_number,
        a.periksa_xpert_id as specimen_xpert,
        a.periksa_lpa_lini2_id as specimen_lpa,
        a.periksa_biakan_id as specimen_biakan,
        a.periksa_paket_standar_id as specimen_cdst,
        a.tgl_pengambilan as specimen_date_collected
        
        ");
        
        $this->sitb->from("ta_permohonan_lab a");
        $this->sitb->where("a.status_terima_id","0");
        $this->sitb->where("a.status_kirim_id","1");
        $this->sitb->where("a.jenis_pengiriman_id","1");
        $this->sitb->where("a.id",$id);
        $this->sitb->join("ta_person b","a.`person_id`=b.`id`");
        $this->sitb->join("ref_unit_tb c","a.fasyankes_id=c.id");
        $this->sitb->join("ref_unit_tb d","a.lab_tujuan_id=d.id");
        $this->sitb->join("ref_provinsi e","b.`provinsi_id`=e.`id`");
        $this->sitb->join("ref_kabupaten f","b.`kabupaten_id`=f.`id`"); 
        return $this->sitb->get()->row();

    }
}