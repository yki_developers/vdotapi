<?php
Class M_about extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function getVersion(){
        return $this->db->get(DB_SYS_VERSION)->row();
    }

}