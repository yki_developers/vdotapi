<?php
Class M_vdot extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($rdata){
        return $this->db->insert(DB_VDOT,$rdata);
    }

    public function getDataVdot($idpasien){
        $this->db->select(DB_VDOT.".*");
        $this->db->select("tanggal_pengobatan,nama_pasien");
        $this->db->where("vdot_idpasien",$idpasien);
        $this->db->order_by("vdot_id","DESC");
        $this->db->from(DB_VDOT);
        $this->db->join(DB_PASIEN,"vdot_idpasien=id");
        return $this->db->get()->result();
    }
}