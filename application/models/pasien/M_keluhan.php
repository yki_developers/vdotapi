<?php
Class M_keluhan extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

    public function insert($rdata){
        return $this->db->insert(DB_KELUHAN,$rdata);
    }

    public function getList($id){
        $this->db->where("keluhan_pasien_id",$id);
        return $this->db->get(DB_KELUHAN)->result();
    }

    public function detail($id){
        $this->db->where("keluhan_id",$id);
        return $this->db->get(DB_KELUHAN)->row();
    }

    public function detailbydate($id,$tgl){
        $this->db->where("keluhan_pasien_id",$id);
        $this->db->where("date(keluhan_tanggal)",$tgl);
        $this->db->order_by("keluhan_tanggal","DESC");
        return $this->db->get(DB_KELUHAN)->result();
    }

    public function update($rdata,$id){
        return $this->db->update(DB_KELUHAN,$rdata,array("keluhan_id"=>$id));
    }

    public function delete($id){
        return $this->db->delete(DB_KELUHAN,array("keluhan_id"=>$id));
    }


}