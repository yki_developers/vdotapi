<?php
Class M_pasien extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getListPasien($rdata){
        $this->db->select("id,no_pasien_etb,nik,nama_pasien,umur,IF(jenis_kelamin='1','P','L') as jenis_kelamin,tanggal_pengobatan");
        $this->db->select("CONCAT('EC',IF(jenis_kelamin='1','P','L'),id) as username,'123' as password");
        $this->db->where($rdata);
        $this->db->where("tanggal_pengobatan IS NOT NULL");
        $this->db->where("status_terahir_etb=''");
        $this->db->where("user_name IS NULL");
        $this->db->from(DB_PASIEN);
        $this->db->join(DB_USER,"user_idpasien=id","LEFT");
        return $this->db->get()->result();
    }

    public function insert($rdata){
    return $this->db->insert(DB_USER,$rdata);
    }



}