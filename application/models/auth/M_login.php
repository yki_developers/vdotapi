<?php
Class M_login extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function tokenGet($Xkey){
        $this->db->select("CONCAT_WS('_',id,user_id,UUID(),'".$Xkey."',level) as token");
        $this->db->where("key",$Xkey);
       $q = $this->db->get(DB_REST_API_KEY);
     return $q->row();
       //return $this->db->get_compiled_select();
    }


    public function authUser($rdata){
        $this->db->where($rdata);
       // return $this->db->get_compiled_select();
        return $this->db->get(DB_USER)->row();
    }

    public function update($username,$rdata){
        return $this->db->UPDATE(DB_USER,$rdata,array("user_name"=>$username));
    }

}