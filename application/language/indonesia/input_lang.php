<?php
/* Language : ID */

//A
$lang['add'] = "Tambah Baru";
$lang['allprovinces'] = "-- Semua Propinsi --";
$lang['alldistricts'] = "-- Semua Kab/Kota --";
$lang['allfacilities'] = "-- Semua Faskes --";
$lang['allExamination'] = "-- Semua Tes --";

///B

//C

//D
$lang['delete'] = "Hapus";
//E
$lang['edit'] = "Ubah";
$lang['error'] = "ERROR";
//F
//G
//H
//I
$lang['invalid'] = "Invalid";
//J
//K
//L
$lang['login']     = "Login";
//M
$lang['month'] = "Bulan";
//N
$lang['negative'] = "Negatif";
$lang['nr'] = "Tidak ada hasil";
//O
//P
$lang['patient'] = "Pasien";
$lang['patient_address'] = "Alamat Pasien";
$lang['patient_name'] = "Nama Pasien";
$lang['patient_id'] = "ID Pasien";
$lang['patient_nid'] = "NIK";
$lang['patient_sex'] = "Jenis Kelamin";
$lang['patient_bday'] = "Tanggal Lahir";
$lang['patient_age'] = "Umur";
$lang['patient_regnas'] = "No. Registrasi";
$lang['patient_med_record'] = "No. Rekam Medis";
$lang['patient_date_confirmed'] = "Tanggal Konfirmasi HIV+";


$lang['pickup_confirmation_date'] ="Tanggal konfirmasi penjemputan";
$lang['pickup_delivered'] ="Sampai tujuan";
//Q
//R
$lang['rs'] = "Rifampicin Sensitif";
$lang['rr'] = "Rifampicin-resistant";
$lang['ri'] = "Rifampicin-indeterminate";

$lang['result_lab_register'] = "No. Register Lab";
$lang['result_date'] = "Tanggal hasil";
$lang["result_micro"] = "Hasil mikroskopis";
$lang["result_sputum_xpert"] = "Hasil Xpert Pemeriksaan Sputum";
$lang["result_stool_xpert"] = "Hasil Xpert Pemeriksaan Stool";
//S
$lang['submit']   = "Simpan";
$lang["search"] = "Cari";
$lang['shipper_company'] = "Perusahaan Kurir";

$lang['specimen_cold_chain'] ="dikirim dengan cold chain?";
$lang["cold_chain_yes"] = "Ya";
$lang["cold_chain_partly"] = "Ya, sebagian";
$lang["cold_chain_no"] = "Tidak";
$lang['specimen_number'] = "No. Persediaan";
$lang['specimen_test_type'] = "Jenis Tes";
$lang['specimen_type'] = "Jenis Spesimen";
$lang['specimen_presumptive_type'] = "Kategori pemeriksaan";
$lang['specimen_type_child'] = "TB anak";
$lang['specimen_type_hiv'] = "Status HIV";
$lang['specimen_type_dm'] = "DM";
$lang['specimen_type_malnutrition'] = "Gizi buruk";
$lang['specimen_type_recurrent_pneumonia'] = "Reccurrent Pneumonia";
$lang['specimen_type_persistent_pneumonia'] = "Persistent Pneumonia";
$lang['specimen_anatomic_loc'] = "Lokasi Anatomi";
$lang['specimen_anatomic_loc_extra'] = "Lokasi";
$lang['specimen_exam_purpose'] = "Usulan Pemeriksaan";
$lang['specimen_fup_test'] = "Follow up test (month)";
$lang['specimen_fup_aftertreatment'] ="Follow up after treatment completion";
$lang['specimen_national_regnum'] = "Register TB Nasional.";
$lang['specimen_district_regnum'] = "No. Register Kab/kota.";

$lang['specimen_type_stool_app'] = "Kondisi Stool";
$lang['specimen_type_sputum_app'] = "Kondisi dahak";

$lang['sex_m'] = "Laki-laki";
$lang['sex_f'] = "Perempuan";

$lang['status_draft'] = "DRAFT";
$lang['status_waiting_confirmation'] = "Menunggu Konfirmasi Penjemputan";
$lang['status_waiting_pickup'] = "Menunggu penjemputan";
$lang['status_picked_up'] = "Paket dibawa oleh kurir";
$lang['status_delivered'] = "Paket sampai tujuan";
$lang['status_received'] = "Paket sudah diterima";
$lang['status_finished'] = "Selesai";
//T
//U
$lang['update'] = "Update";
//V
//W
//X
//Y
//Z


//added 09/07/2020
$lang['type_hospital'] = "Rumah sakit";
$lang['type_phc']  = "Puskesmas";
$lang['type_clinic'] = "Klinik";

$lang['change_password'] = "Ganti Password";
$lang["old_password"] = "Password Lama";
$lang["new_password"] = "Password Baru";

$lang['main_task'] = "Tupoksi";

$lang['presumptive_DSTB'] = "Terduga TB SO";
$lang['presumptive_DRTB'] = "Terduga TB RO";
$lang['patient_DSTB'] = "Pasien TB SO";
$lang['patient_DRTB'] = "Pasien TB RO";
$lang['patient_phone_number'] = "No. Telp";
$lang['pickup_confirmation'] = "Konfirmasi penjemputan";

$lang['result_mtb_result'] = "Hasil MTB";
$lang['result_mtb_qty'] = "Qty";
$lang['result_rr_result'] = "Hasil RR";

$lang["age_year"] = "Tahun";
$lang["age_month"] = "Bulan";
$lang["specimen_condition"] = "Kondisi Spesimen";
