<?php
/* Language : ID */

//A
$lang['add'] = "Add new";
$lang['allprovinces'] = "-- All Province --";
$lang['alldistricts'] = "-- All District --";
$lang['allfacilities'] = "-- All Facility --";
$lang['allExamination'] = "-- All Test --";

///B

//C

//D
$lang['delete'] = "Delete";
//E
$lang['edit'] = "Edit";
$lang['error'] = "ERROR";
//F
//G
//H
//I
$lang['invalid'] = "Invalid";
//J
//K
//L
$lang['login']     = "Log in";
//M
$lang['month'] = "Month";
//N
$lang['negative'] = "Negative";
$lang['nr'] = "No Result";
//O
//P
$lang['patient'] = "Patient";
$lang['patient_address'] = "Patient Address";
$lang['patient_name'] = "Name of the patient";
$lang['patient_id'] = "Patient ID";
$lang['patient_nid'] = "NID";
$lang['patient_sex'] = "Sex";
$lang['patient_bday'] = "Date of Birth";
$lang['patient_age'] = "Age";
$lang['patient_regnas'] = "Patient Registration No.";
$lang['patient_med_record'] = "Medical Record No.";
$lang['patient_date_confirmed'] = "HIV+ confirmed date";


$lang['pickup_confirmation_date'] ="Pick up confirmation date";
$lang['pickup_delivered'] ="Delivered";
//Q
//R
$lang['rs'] = "Rifampicin-sensitive";
$lang['rr'] = "Rifampicin-resistant";
$lang['ri'] = "Rifampicin-indeterminate";

$lang['result_lab_register'] = "Lab Registration Number";
$lang['result_date'] = "Result Date";
$lang["result_micro"] = "Microscopy result";
$lang["result_sputum_xpert"] = "Sputum Xpert Result";
$lang["result_stool_xpert"] = "Stool Xpert Result";
//S
$lang['submit']   = "Save";
$lang["search"] = "Search";
$lang['shipper_company'] = "Courier Company";

$lang['specimen_cold_chain'] ="Transported in cold chain?";
$lang["cold_chain_yes"] = "Yes";
$lang["cold_chain_partly"] = "Yes, partly";
$lang["cold_chain_no"] = "No";
$lang['specimen_number'] = "Specimen ID/number";
$lang['specimen_test_type'] = "Type of test";
$lang['specimen_type'] = "Type of specimen";
$lang['specimen_presumptive_type'] = "Case category";
$lang['specimen_type_child'] = "Childhood TB";
$lang['specimen_type_hiv'] = "HIV Status";
$lang['specimen_type_dm'] = "DM";
$lang['specimen_type_malnutrition'] = "Malnutrition";
$lang['specimen_type_recurrent_pneumonia'] = "Reccurrent Pneumonia";
$lang['specimen_type_persistent_pneumonia'] = "Persistent Pneumonia";
$lang['specimen_anatomic_loc'] = "Anatomic location";
$lang['specimen_anatomic_loc_extra'] = "Anatomic location";
$lang['specimen_exam_purpose'] = "Examination purpose";
$lang['specimen_fup_test'] = "Follow up test (month)";
$lang['specimen_fup_aftertreatment'] ="Follow up after treatment completion";
$lang['specimen_national_regnum'] = "National TB Register No.";
$lang['specimen_district_regnum'] = "Woreda TB Register No.";

$lang['specimen_type_stool_app'] = "Stool condition";
$lang['specimen_type_sputum_app'] = "Sputum condition";

$lang['sex_m'] = "Male";
$lang['sex_f'] = "Female";

$lang['status_draft'] = "DRAFT";
$lang['status_waiting_confirmation'] = "Waiting for pickup confirmation";
$lang['status_waiting_pickup'] = "Waiting for pickup";
$lang['status_picked_up'] = "Package collected by courier";
$lang['status_delivered'] = "Package delivered";
$lang['status_received'] = "Package received";
$lang['status_finished'] = "Finished";
//T
//U
$lang['update'] = "Update";
//V
//W
//X
//Y
//Z


//added 09/07/2020
$lang['type_hospital'] = "Hospital";
$lang['type_phc']  = "Primary Health Center";
$lang['type_clinic'] = "Clinic";

$lang['change_password'] = "Change Password";
$lang["old_password"] = "Old Password";
$lang["new_password"] = "New Password";

$lang['main_task'] = "Main task";

$lang['presumptive_DSTB'] = "Presumptive of DS TB";
$lang['presumptive_DRTB'] = "Presumptive of DR TB";
$lang['patient_DSTB'] = "DS TB Patient";
$lang['patient_DRTB'] = "DR TB Patient";
$lang['patient_phone_number'] = "Phone Number";
$lang['pickup_confirmation'] = "Pickup Plan confirmation";

$lang['result_mtb_result'] = "MTB Result";
$lang['result_mtb_qty'] = "Quantity";
$lang['result_rr_result'] = "RR Result";

$lang["age_year"] = "Years";
$lang["age_month"] = "Months";
$lang["specimen_condition"] = "Specimen condition";