<?php
/* Language : ID */
//A
$lang['action'] = "Action";
$lang['add_network'] = "Laboratory Network";
$lang['add_specimen'] = "Adding specimen";
$lang['address'] = "Address";



//B

//C
$lang['condition_bad'] = "Damaged";
$lang['condition_good'] = "Good";
//D
$lang['dashboard'] = "Dashboard";
$lang['data_form'] = "Data Form";
$lang['dateformat'] ="Date Format";
$lang['district'] = "Woreda";
$lang['district_code'] = "Woreda code";
$lang['district_name'] = "Woreda name";
$lang['detination'] = "Destination";
$lang['detail'] = "Detail";
$lang['doctor_name'] = "Doctor in charge";
$lang['delivered'] = "Arrived";

//E
$lang['examinationType'] = "Type of test";


//F
$lang['forgetpassword'] = "Forgot Password";
$lang['finish'] = "Finished";
$lang['feedback'] = "Feedback";

//G
$lang['geo_structure'] = "Geographical Setting";
$lang['group'] = "Group";
$lang['group_id'] = "Group ID";

//H
$lang['health_facility'] = "Health Facility";
$lang['home'] = "Home";
$lang['healthfacility_name'] = "Name of Health Facility";
$lang['healthfacility_code'] = "Code of Health Facility";
$lang['healthfacility_referral'] = "Referral Health Facility";

$lang['healthfacility_type'] = "Type of Facility";
$lang['healthfacility_type_hospital'] = "Hospital";
$lang["healthfacility_type_phc"] = "Primary Health Care";
$lang['healthfacility_type_clinic'] = "Clinic";

//I

$lang['internal'] = "Internal Examination Request";
$lang['internal_pdp'] ="Examination Request";
$lang["internal_result"] = "Result";
//J

//K

//L
$lang['logout'] = "Sign Out";

//M
$lang['monitoring']="Follow Up";
$lang['microscopic'] = "Microscopic";


//N
$lang['number'] = "No";
$lang['network'] = "Health Service Network";
$lang["network_lab"] = "Laboratory Network";
$lang["network_treatment"] = "Treatment Network";
$lang["network_origin"] = "Origin Health Facility";

//O
$lang['order'] = "Order";
$lang['order_number'] = "Order Number";
$lang['order_date'] = "Date of Order";
$lang['order_destination'] = "Destination";
$lang['order_courier'] = "Courier";
$lang['order_status'] = "Order status";
$lang['origin'] = "Sender";
$lang['order_origin'] = "Sender Health Facility";
$lang['order_rejected'] = "Request Denied";
$lang["order_approved"] = "Request Accepted";
$lang["order_num_specimen"] = "Specimen number";
$lang['order_reason'] = "Reason for refusal";
$lang['order_confirmation'] = "Order confirmation";
//P
$lang['password'] = "Password";
$lang['patient_reg_nation'] = "National registration number";
$lang['periode'] = "Period";
$lang['profile'] = "Profile";
$lang['province'] = "Zone";
$lang['province_code'] = "Zone Code";
$lang['province_name'] = "Zone Name";
$lang['pickup'] = "Collection";
$lang['pickup_confirmation'] = "Confirmation";
//new
$lang['phone_number'] = "Phone number";

//Q

//R
$lang["recipient_facility"] = "Recipient Health Facility";
$lang['ReferenceData'] = "Data Reference";
$lang['rememberpassword'] = "Remember Password";
$lang['report'] = "Report";
$lang['receive'] = "Received Package";
$lang['confirmation'] = "Confirmation";
$lang['result'] ="Result";
$lang['result_absolut'] = "Absolute Result";
$lang['rejected'] = "Denied";
$lang['approved'] = "Accepted";

$lang['report_sender_facility'] = "Sender Health Facility";
$lang['report_exam_facility'] = "Receipt Health Facility";
$lang['report_month_to'] = "Month (of treatment)";
$lang['report_periode'] = "Test period";
$lang["report_year"] = "Year";
$lang["report_month"] = "Month (of treatment)";
$lang["report_date"] = "Last examination date";
$lang["result_view"] = "See result";
$lang["result_conclusion"] = "Result summary";
//S
$lang['specimen_id'] = "Specimen ID";
$lang['specimen_art_date'] = "ART starting date";
$lang['specimen_date_collected'] = "Sample Collection Date";
$lang['specimen_test_type'] = "Examination Category";
$lang['specimen_exam_date'] = "Examination Date";
$lang['specimen_date_release'] = "Result Date";

$lang['sender_facilities'] = "Sender Health Facility";
$lang['shipper_company'] = "Courier Company";
$lang['shipper_code'] = "Courier Code";
$lang['shipper_name'] = "Name of Courier";
$lang['specimen']="Specimen";
$lang['SwitchLanguage'] = "Choose Language";
$lang['subdistrict'] = "Kebele";
$lang['subdistrict_code'] = "Kebele code";
$lang['subdistrict_name'] = "Kebele name";

//T

//U
$lang['username'] = "Username";
$lang['UserData'] = "User Data";
$lang['unit'] = "Unit";
$lang['user_management'] = "User settings";
$lang['user'] = "User";
$lang['user_group'] = "Group Settings";
$lang['user_role'] = "Access Setting";
$lang['user_fname'] = "First Name";
$lang['user_lname'] = "Last Name";
$lang['user_unit'] = "User Unit";

$lang['username_group'] = "Group User";
$lang['user_hf'] = "Health worker";
$lang['user_kurir'] = "Courier Officer";
$lang['user_fullname'] = "Full Name";



$lang['user_group_superuser'] = "Super User";
$lang['user_group_moh'] = "National/Central";
$lang['user_group_dho'] = "Woreda Health Office";
$lang['user_group_pho'] = "Zone Health Office";
$lang['user_group_moh'] = "Ministry of Health";
$lang['user_group_facility'] = "Health Facility";
$lang['user_group_courier'] = "Courier";
$lang['user_group_authority'] = "User authority";
$lang['user_group_fac_2'] = "Recipient Health Facility";
$lang['user_group_fac_1'] = "Sender Health Facility"; 
$lang['user_group_fac_3'] = "Sender-and-Recipient";
$lang['user_hf_group'] = "Tugas Pokok dan Fungsi";
$lang["user_hf_group_1"] = "LAB Faskes Pengampu";
$lang['user_hf_group_2'] = "PDP Faskes Pengampu";
$lang['user_hf_group_3'] = "Lab Faskes Satelit";
$lang['user_hf_group_4'] = "PDP Faskes Satelit";
$lang['user_hf_group_5'] = "Lab Faskes Rujukan";

//V
$lang['vl_report'] = "Laporan Pemeriksaan VL";
$lang['vl_monitoring'] = "Monitoring Pengiriman";
$lang['eid_report'] = "Laporan Pemeriksaan EID";

//W
$lang['welcome'] = "Welcome";

//X
$lang['xpert'] = "TCM";

//Y

//Z



$lang['dashboard_num_specimen_ref'] = "Number of <br>specimens referred";
$lang['dashboard_num_shipment_distpatched'] = "Number of <br>specimen received";
$lang['dashboard_num_arrived'] = "Number of <br>shipments arrived (48 Hours)";
$lang['dashboard_num_result'] = "Number of <br>result delivered";
$lang["dashboard_num_participants"] = "Number of <br>HCF Referring Spec";





$lang['specimen_report'] = "Report of <br>Specimen Referral";
$lang['shipment_report'] = "Report of shipment ";
$lang['report_tat'] = "Turn around Time";
$lang["start_date"] = "Start Date";
$lang["end_date"] = "End Date";

$lang["report_pickup_site"] = "Pick Up Sites";
$lang["report_recipient_site"] = "Recipient Sites";
$lang["report_number_of_specimen"] = "Number Of Specimens";
$lang["report_pickup_date"] = "Date of Pick up";
$lang["report_pickup_time"] = "Time of Pick up";
$lang["report_pickup_duration"] = "Pick up Duration";
$lang["report_date_delivered"] = "Date of Drop-off";
$lang["report_time_delivered"] = "Time of Drop-off";
$lang["report_date_received"] = "Date specimen package";
$lang["report_shipping_duration"] = "Shipping Duration";
$lang["order_monitoring"] = "Order Monitoring";
$lang["filter"] = "Filter";
$lang["data_table"] = "Data Table";
$lang["yes"] = "Ya";
$lang["no"] = "Tidak";


































