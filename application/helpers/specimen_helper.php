<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('hivStatus')){
    function hivStatus($val){
        switch($val){
            case 1:$val = "Positif";break;
            case 2:$val = "Negative";break;
            case 3:$val = "Unknown";break;
        }
        return $val;
    }
}

if ( ! function_exists('yes_no'))
{
    function yes_no($val){
        switch($val){
            case 1:$val= "YA";break;
            case 0:$val="Tidak";break;
            default:$val="N/A";break;
        }
        return $val;
    }
}

if ( ! function_exists('xpert_result'))
{
    function xpert_result($val){
        switch($val){
            case 1:$val= "negative";break;
            case 2:$val="rs";break;
            case 3:$val="rr";break;
            case 4:$val="ri";break;
            case 5:$val="invalid";break;
            case 6:$val="error";break;
            case 7:$val="nr";break;
        }
        return $val;
    }
}

if(!function_exists('conclusion_result')){
    function conclusion_result($val){
        switch($val){
            case 1:$val = "MTB NOT DETECTED";break;
            case 2:$val = "MTB detected RIF res not detected";break;
            case 3:$val = "MTB detected RIF resist  detected";break;
            case 4:$val = "MTB detected RIF res indeterminate";break;
        }
        return $val;
    }
}

if(!function_exists('dashboard_tat')){
    function dashboard_tat($val){
        switch($val){
            case 1:$val = "0 - 6 Hours";break;
            case 2:$val = "6 - 12 Hours";break;
            case 3:$val = "12 - 24 Hours";break;
            case 4:$val = "24 - 48 Hours";break;
            case 5:$val = "> 48 Hours";break;
        }
        return $val;

    }
}

if(!function_exists('dashboard_age')){
    function dashboard_age($val){
        switch($val){
            case 1:$val = "< 5 Y.O";break;
            case 2:$val = "5 - 14 Y.O";break;
        }
        return $val;

    }
}


if ( ! function_exists('mtb_result'))
{
    function mtb_result($val){
        switch($val){
            case 1:$val= "MTB DETECTED";break;
            case 2:$val="MTB NOT DETECTED";break;
        }
        return $val;
    }
}

if ( ! function_exists('qty_result'))
{
    function qty_result($val){
        switch($val){
            case 1:$val= "HIGH";break;
            case 2:$val="MEDIUM";break;
            case 3:$val= "LOW";break;
            case 4:$val="VERY LOW";break;
            case 5:$val="TRACE";break;
        }
        return $val;
    }
}

if( ! function_exists('rr_result'))
{
    function rr_result($val){
        switch($val){
            case 1:$val= "RR Detected";break;
            case 2:$val="RR Not Detected";break;
            case 3:$val= "RR Indeterminate";break;
        }
        return $val;
    }
}


if(!function_exists('presumptive_type')){
    function presumptive_type($val){
        switch($val){
            case 1:$val="presumptive_DSTB";break;
            case 2:$val="presumptive_DRTB";break;
            case 3:$val="patient_DSTB";break;
            case 4:$val="patient_DRTB";break;
        }
        return $val;
    }
}

if(!function_exists('stool_app')){
    function stool_app($val){
        switch($val){
            case 1:$val="Formed";break;
            case 2:$val="Unformed";break;
            case 3:$val="liquid";break;
        }
        return $val;
    }
}


if(!function_exists('sputum_app')){
    function sputum_app($val){
        switch($val){
            case 1:$val="Mucoid purulent";break;
            case 2:$val="Bloodstreak";break;
            case 3:$val="Saliva";break;
        }
        return $val;
    }
}

if(!function_exists('cold_chain')){
    function cold_chain($val){
        switch($val){
            case 1:$val="cold_chain_yes";break;
            case 2:$val="cold_chain_partly";break;
            case 3:$val="cold_chain_no";break;
        }
        return $val;
    }
}

if(!function_exists('micro_result')){
    function micro_result($val){
        switch($val){
            case 1:$val="+3";break;
            case 2:$val="+2";break;
            case 3:$val="+1";break;
            case 4:$val="1-9";break;
            case 5:$val="Negative";
        }
        return $val;
    }
}

if(!function_exists('specimen_type')){
    function specimen_type($val){
        switch($val){
            case 1:$val="sputum";break;
            case 2:$val="stool";break;
        }
        return $val;
    }
}

if(!function_exists('specimen_test')){
    function specimen_test($val){
        switch($val){
            case 1:$val="AFB test (Microscopic)";break;
            case 2:$val="Rapid Molecular Test (Xpert)";break;
        }
        return $val;
    }
}

if(!function_exists('anatomic_location')){
    function anatomic_location($val){
        switch($val){
            case 1:$val="Pulmonary";break;
            case 2:$val="Extrapulmonary";break;
        }
        return $val;
    }
}


if(!function_exists('exam_purpose')){
    function exam_purpose($val){
        switch($val){
            case 1:$val="Diagnoses - DS TB";break;
            case 2:$val="Diagnoses - DR TB";break;
            case 3:$val="Follow UP";break;
            case 4:$val="Follow Up After Completion";break;
        }
        return $val;
    }
}

if(!function_exists('specimen_sex')){
    function specimen_sex($val){
        switch($val){
            case 1:$val="Male";break;
            case 2:$val="Female";break;

        }
        return $val;
    }
}




if(!function_exists('order_status')){
    function order_status($val){
        switch($val){
            case 0:$val="status_draft";break;
            case 1:$val="status_waiting_confirmation";break;
            case 2:$val="status_waiting_pickup";break;
            case 3:$val="status_picked_up";break;
            case 4:$val="status_delivered";break;
            case 5:$val="status_received";break;
            case 6:$val="status_finished";break;

        }
        return $val;
    }
}


if(!function_exists('specimen_test_periode')){
    function specimen_test_periode($val){
       if($val>=3 && $val<=9){
           $val = "6 bln";
       }elseif($val>=10 && $val<=18){
           $val = "12 bln";
       }elseif($val>=19 && $val<=30){
        $val = "24 bln";
    }elseif($val>=31 && $val<=42){
        $val = "36 bln";
    }elseif($val>=43 && $val<=54){
        $val = "48 bln";
    }elseif($val>=55 && $val<=66){
        $val = "60 bln";
    }elseif($val>=67){
        $val = "72 bln";
    }
        return $val;
    }
}



if(!function_exists('db_date')){
    function db_date($val){
        $dt = explode($val,"-");
        $dbformat = $dt[2]."-".$dt[1]."-".$dt[0];
        return $dbformat;
    }
}

if(!function_exists('local_date')){
    function local_date($val){
        $dt = explode($val,"-");
        $dbformat = $dt[2]."-".$dt[1]."-".$dt[0];
        return $dbformat;
    }
}

if(!function_exists('remove_data')){
    function remove_data($array,$keys){
        foreach($keys as $key){
            unset($array[$key]);
        }
        return $array;
    }
}




if(!function_exists('duration')){
    function duration($val){
        if($val<60){
            if($val>0 && $val<10){
                $m="0".$val;
            }else{
                $m=$val;
            }
            $duration = "00:".$m;
        }else{
            $m = $val % 60;
            $jm = ($val-$m)/60;
            $duration=$jm.":".$m;
        }
        return $duration;
    }
}

if(!function_exists('getDate')){
    function getDate($val){
        $d = substr($val,1,10);
        
        return $d;
    }
}
