<?php
Class Crontab extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_crontab');
    }

    private function send_notif($uid,$nama,$waktu){
        $curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://empatitb.id/api/ec/notif/'.$uid,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "message":"Pasien TB atas nama '.$nama.'  sudah  '.$waktu.' jam, dari jadwalnya belum mengirimkan video menelan obat",
    "title":"Reminder Pengiriman Video"
}',
  CURLOPT_HTTPHEADER => array(
    'xkey: 13666',
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);
curl_close($curl);
return true;
    }

    public function index(){
       $mdata = $this->M_crontab->listReminder();
       foreach($mdata as $list){
           $nama = $list->nama_pasien;
           $uid = $list->user_idpasien;
           $jam = $list->jadwal;
           if($this->send_notif($uid,$nama,$jam)){
           $this->M_crontab->updateReminder($uid);
           echo $uid." done!<br>";
           }
       }
    }
}