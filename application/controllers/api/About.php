<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;


Class About extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('M_about');
    }


 

    public function version_get(){
        $result = $this->M_about->getVersion();
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);

    }




}