<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Keluhan extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pasien/M_keluhan");
    }

    public function index_post(){
              
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
           // $string=$this->input->post('file_video');
        }else{
            $rdata = $row;
            //$string=$row['file_video'];
        }
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_keluhan->insert($rdata)){
        
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    
    }



    public function index_put($id){
              
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
       $rdata = $this->put();
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_keluhan->update($rdata,$id)){
        
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    
    }




    public function index_get($id){
              
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_keluhan->getList($id)){
        
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    
    }


    public function detail_get($id){
              
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_keluhan->detail($id)){
        
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    
    }



    public function detailbydate_get($userid,$tgl){
              
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_keluhan->detailbydate($userid,$tgl)){
        
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    
    }




    public function index_delete($id){
              
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_keluhan->delete($id)){
        
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    
    }
}