<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Login extends REST_controller{ 

public function __construct(){
    parent::__construct();
    $this->load->model('auth/M_login');

}


public function index_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);

    if(!$this->input->post()){
$authUser = array("user_name"=>$row['user_name'],"user_password"=>md5($row['user_password']));
$playerid = $row['user_playerid'];
    }else{
$authUser =  array("user_name"=>$this->input->post('user_name'),"user_password"=>md5($this->input->post('user_password')));
$playerid = $this->input->post('user_playerid');
    }
$userId = implode(":",$authUser);
   

    $headers=$this->input->request_headers();
    if($dataq = $this->M_login->tokenGet($headers['Xkey'])){
       
       
        if($id=$this->M_login->authUser($authUser)){
            $src_token = $dataq->token.":".$userId;
            $token = AUTHORIZATION::generateToken($src_token);
            //$id->role = $this->M_login->getDataRoleList($id->group_id);
            $id->token = $token;
            if($id->user_status_aktif=='0'){
                $lastlog = array("user_playerid"=>$playerid,"user_lastlogin"=>date("Y-m-d H:i:s"),"user_status_aktif"=>"1","user_firstlogin"=>date("Y-m-d H:i:s"));

            }else{
            $lastlog = array("user_playerid"=>$playerid,"user_lastlogin"=>date("Y-m-d H:i:s"));
            }
            $this->M_login->update($id->user_name,$lastlog);
            $ret = array(
                "status"=>REST_Controller::HTTP_OK,
                "error"=>null,
                "message"=>"Accepted",
                "response"=>$id
            );
            $this->set_response($ret, REST_Controller::HTTP_OK);
        }else{

            $ret = array(
                "status"=>REST_Controller::HTTP_OK,
                "error"=>null,
                "message"=>"Accepted",
                "response"=>false
            );
            
            $this->set_response($ret, REST_Controller::HTTP_OK);

        }   
    
    }
	return;	

}


public function logout_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
if($this->input->post()){
    $rdata= $this->input->post("user_name");
}else{
    $rdata = $row['user_name'];
}

$playerid= array("user_playerid"=>null);
if($this->M_login->update($rdata,$playerid)){
    $ret = array(
        "status"=>REST_Controller::HTTP_OK,
        "error"=>null,
        "message"=>"Acceptep",
        "response"=>true
    );
    $this->set_response($ret, REST_Controller::HTTP_OK);
}


}

}

