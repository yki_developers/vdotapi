<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Pasien extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pasien/M_pasien");
    }

    public function index_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $rdata = $this->input->post();
    }else{
        $rdata = $row;
    }



   if($result = $this->M_pasien->getListPasien($rdata)){
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }
   
    return;

    }


    public function add_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
            $password=$this->input->post('user_password');
        }else{
            $rdata = $row;
            $password = $row['user_password'];
        }




        unset($rdata['user_password']);
        $rdata['user_password']=md5($password);

        if($result=$this->M_pasien->insert($rdata)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  

        }else{

            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);  

        }

        return;

    }
}