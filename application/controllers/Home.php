<?php
Class Home extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
      if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }
        $this->load->model('sys/M_module');
        $this->load->model("transactional/M_order");
    }

    public function index(){
        //echo "xhome";
        //print_r($this->session->userdata());
    $data['parent'] = $this->M_module->getParentModule($this->session->userdata('user_group'));
    $data['userdata'] = $this->session->userdata();
    $data['neworder'] = $this->M_order->getOrderInfo($this->session->userdata('user_unit'),'1');
    $data['pickup'] = $this->M_order->getOrderInfo($this->session->userdata('user_unit'),'2');
    $data['ondelivery'] = $this->M_order->getOrderInfo($this->session->userdata('user_unit'),'3');
    $data['delivered'] = $this->M_order->getOrderInfo($this->session->userdata('user_unit'),'4');
    $data['result'] = $this->M_order->getOrderInfo($this->session->userdata('user_unit'),'5');
    $data['orderread'] = $this->M_order->orderRead($this->session->userdata('user_unit'));


   $this->template->renderpage('frontpage/home',$data);
    }

}