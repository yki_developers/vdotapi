<?php
class LanguageLoader
{
    
    function initialize() {
        $langs = array("message","input","label");
        $ci =& get_instance();
        $ci->load->helper('language');
        $siteLang = $ci->session->userdata('site_lang');
        if ($siteLang) {
            $ci->lang->load($langs,$siteLang);
        } else {
            $ci->lang->load($langs,'indonesia');
        }
    }
}
?>